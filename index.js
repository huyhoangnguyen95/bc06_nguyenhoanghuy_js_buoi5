// Bài 1

function chonKhuVuc(khuVuc) {
  switch (khuVuc) {
    case "kv":
      return 0;
    case "a":
      return 2;
    case "b":
      return 1;
    case "c":
      return 0.5;
  }
}

function chonDoiTuong(doiTuong) {
  switch (doiTuong) {
    case "0":
      return 0;
    case "1":
      return 2.5;
    case "2":
      return 1.5;
    case "3":
      return 1;
  }
}

function inKetQua() {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var khuVuc = document.getElementById("selectKhuVuc").value;
  var doiTuong = document.getElementById("seclectDoiTuong").value;
  var diemThuNhat = document.getElementById("txt-diem-thu-nhat").value * 1;
  var diemThuHai = document.getElementById("txt-diem-thu-hai").value * 1;
  var diemThuBa = document.getElementById("txt-diem-thu-ba").value * 1;

  // điểm tương ứng khu vực
  var khuVucValue = chonKhuVuc(khuVuc);
  console.log("khuVucValue: ", khuVucValue);

  // điểm tương ứng đối tượng
  var doiTuongValue = chonDoiTuong(doiTuong);
  console.log("doiTuongValue: ", doiTuongValue);

  var total =
    diemThuNhat + diemThuHai + diemThuBa + khuVucValue + doiTuongValue;

  if (diemThuNhat == 0 || diemThuHai == 0 || diemThuBa == 0) {
    console.log("bạn đã rớt do có điểm bằng 0");
    document.getElementById(
      "result1"
    ).innerHTML = ` Bạn đã rớt do có điểm bằng 0 `;
  } else {
    if (total >= diemChuan) {
      document.getElementById(
        "result1"
      ).innerHTML = ` Bạn đã đậu. Tổng điểm: ${total} `;
    } else {
      document.getElementById(
        "result1"
      ).innerHTML = ` Bạn đã rớt. Tổng điểm: ${total} `;
    }
  }

  // if (diemThuNhat !== 0 && diemThuHai !== 0 && diemThuBa !== 0) {
  //   if (total >= diemChuan) {
  //     console.log("Đậu");
  //   } else {
  //     console.log("Rớt");
  //   }
  // } else {
  //   console.log("bạn đã rớt do có điểm bằng 0");
  // }
}

//  bài 2

function tinhTienDien() {
  var hoTen = document.getElementById("txt-ten").value;
  var soKw = document.getElementById("txt-so-kw").value * 1;
  var total = 0;

  if (0 <= soKw && soKw <= 50) {
    total = soKw * 500;
    console.log("th1");
  } else if (soKw <= 100) {
    total = 50 * 500 + (soKw - 50) * 650;
    console.log("th2");
  } else if (soKw <= 200) {
    total = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
    console.log("th3");
  } else if (soKw <= 350) {
    total = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
    console.log("th4");
  } else {
    total = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
    console.log("th5");
  }
  total = new Intl.NumberFormat("vn-VN").format(total);
  document.getElementById("result2").innerHTML = ` 
  Họ tên : ${hoTen} ; Tiền điện : ${total}
  `;
}

// bài 3

function tinhTienThue() {
  var hoTen = document.getElementById("txt-name").value;
  var tongThuNhap = document.getElementById("txt-tong-thu-nhap").value * 1;
  var soNguoiPhuThuoc = document.getElementById("txt-so-nguoi").value * 1;
  var thuNhapChiuThue;

  if (tongThuNhap <= 60e6) {
    thuNhapChiuThue = (tongThuNhap - 4e6 - soNguoiPhuThuoc * 1600000) * 0.05;
  } else if (tongThuNhap <= 120e6) {
    thuNhapChiuThue =
      60e6 * 0.05 +
      (tongThuNhap - 60e6 - 4e6 - soNguoiPhuThuoc * 1600000) * 0.1;
    console.log("thuNhapChiuThue: ", thuNhapChiuThue);
  } else if (tongThuNhap <= 210e6) {
    thuNhapChiuThue *= 0.15;
  } else if (tongThuNhap <= 384e6) {
    thuNhapChiuThue *= 0.2;
  } else if (tongThuNhap <= 624e6) {
    thuNhapChiuThue *= 0.25;
  } else if (tongThuNhap <= 960e6) {
    thuNhapChiuThue *= 0.3;
  } else if (tongThuNhap > 960e6) {
    thuNhapChiuThue *= 0.35;
  }
  thuNhapChiuThue = new Intl.NumberFormat("vn-VN").format(thuNhapChiuThue);
  document.getElementById("result3").innerHTML = ` 
  Thu nhập chịu thuế là: ${thuNhapChiuThue} 
  `;
}

// bài 4

function changeValuable() {
  var khachHang = document.getElementById("seclectKhachHang").value;
  if (khachHang == "2") {
    document.getElementById("demo").style.display = "block";
  } else {
    document.getElementById("demo").style.display = "none";
  }
}
changeValuable();

const PHI_HOA_DON_NHA_DAN = 4.5;
const PHI_CO_BAN_NHA_DAN = 20.5;
const KENH_CAO_CAP_NHA_DAN = 7.5;
const PHI_HOA_DON_DOANH_NGHIEP = 15;
const PHI_CO_BAN_DOANH_NGHIEP = 75;
const KENH_CAO_CAP_DOANH_NGHIEP = 50;

function tinhTienCap() {
  var loaiKhachHang = document.getElementById("seclectKhachHang").value;
  var maKhachHang = document.getElementById("txt-ma-KH").value;
  var soKenhCaoCap = document.getElementById("txt-so-kenh").value * 1;
  var soKetNoi = document.getElementById("txt-so-ket-noi").value * 1;
  var total = 0;

  switch (loaiKhachHang) {
    case "1": {
      total =
        PHI_HOA_DON_NHA_DAN +
        PHI_CO_BAN_NHA_DAN +
        KENH_CAO_CAP_NHA_DAN * soKenhCaoCap;
      break;
    }
    case "2": {
      if (0 < soKetNoi && soKetNoi <= 10) {
        total =
          PHI_HOA_DON_DOANH_NGHIEP +
          PHI_CO_BAN_DOANH_NGHIEP +
          KENH_CAO_CAP_DOANH_NGHIEP * soKenhCaoCap;
      } else {
        total =
          PHI_HOA_DON_DOANH_NGHIEP +
          PHI_CO_BAN_DOANH_NGHIEP +
          (soKetNoi - 10) * 5 +
          KENH_CAO_CAP_DOANH_NGHIEP * soKenhCaoCap;
      }
    }
  }
  document.getElementById("result4").innerHTML = ` 
  Mã khách hàng : ${maKhachHang} ; Tiền cáp là : $${total} 
  `;
}
